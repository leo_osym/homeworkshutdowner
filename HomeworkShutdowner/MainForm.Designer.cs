﻿namespace HomeworkShutdowner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonLock = new System.Windows.Forms.Button();
            this.buttonLogOff = new System.Windows.Forms.Button();
            this.buttonHibernate = new System.Windows.Forms.Button();
            this.buttonSleep = new System.Windows.Forms.Button();
            this.buttonRestart = new System.Windows.Forms.Button();
            this.buttonShutdown = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // buttonLock
            // 
            this.buttonLock.Image = global::HomeworkShutdowner.Properties.Resources.Other_Power_Lock_Metro_icon__1_;
            this.buttonLock.Location = new System.Drawing.Point(203, 99);
            this.buttonLock.Name = "buttonLock";
            this.buttonLock.Size = new System.Drawing.Size(64, 64);
            this.buttonLock.TabIndex = 5;
            this.toolTip.SetToolTip(this.buttonLock, "Lock");
            this.buttonLock.UseVisualStyleBackColor = true;
            this.buttonLock.Click += new System.EventHandler(this.buttonLock_Click);
            // 
            // buttonLogOff
            // 
            this.buttonLogOff.Image = global::HomeworkShutdowner.Properties.Resources.Other_Power_Log_Off_Metro_icon;
            this.buttonLogOff.Location = new System.Drawing.Point(105, 99);
            this.buttonLogOff.Name = "buttonLogOff";
            this.buttonLogOff.Size = new System.Drawing.Size(64, 64);
            this.buttonLogOff.TabIndex = 4;
            this.toolTip.SetToolTip(this.buttonLogOff, "Log Off");
            this.buttonLogOff.UseVisualStyleBackColor = true;
            this.buttonLogOff.Click += new System.EventHandler(this.buttonLogOff_Click);
            // 
            // buttonHibernate
            // 
            this.buttonHibernate.Image = global::HomeworkShutdowner.Properties.Resources.Other_Power_Hibernate_Metro_icon;
            this.buttonHibernate.Location = new System.Drawing.Point(12, 99);
            this.buttonHibernate.Name = "buttonHibernate";
            this.buttonHibernate.Size = new System.Drawing.Size(64, 64);
            this.buttonHibernate.TabIndex = 3;
            this.toolTip.SetToolTip(this.buttonHibernate, "Hibernate");
            this.buttonHibernate.UseVisualStyleBackColor = true;
            this.buttonHibernate.Click += new System.EventHandler(this.buttonHibernate_Click);
            // 
            // buttonSleep
            // 
            this.buttonSleep.Image = global::HomeworkShutdowner.Properties.Resources.Other_Power_Sleep_Metro_icon;
            this.buttonSleep.Location = new System.Drawing.Point(203, 12);
            this.buttonSleep.Name = "buttonSleep";
            this.buttonSleep.Size = new System.Drawing.Size(64, 64);
            this.buttonSleep.TabIndex = 2;
            this.toolTip.SetToolTip(this.buttonSleep, "Sleep");
            this.buttonSleep.UseVisualStyleBackColor = true;
            this.buttonSleep.Click += new System.EventHandler(this.buttonSleep_Click);
            // 
            // buttonRestart
            // 
            this.buttonRestart.Image = global::HomeworkShutdowner.Properties.Resources.Other_Power_Restart_Metro_icon;
            this.buttonRestart.Location = new System.Drawing.Point(105, 12);
            this.buttonRestart.Name = "buttonRestart";
            this.buttonRestart.Size = new System.Drawing.Size(64, 64);
            this.buttonRestart.TabIndex = 1;
            this.toolTip.SetToolTip(this.buttonRestart, "Restart");
            this.buttonRestart.UseVisualStyleBackColor = true;
            this.buttonRestart.Click += new System.EventHandler(this.buttonRestart_Click);
            // 
            // buttonShutdown
            // 
            this.buttonShutdown.Image = global::HomeworkShutdowner.Properties.Resources.Other_Power_Shut_Down_Metro_icon;
            this.buttonShutdown.Location = new System.Drawing.Point(12, 12);
            this.buttonShutdown.Name = "buttonShutdown";
            this.buttonShutdown.Size = new System.Drawing.Size(64, 64);
            this.buttonShutdown.TabIndex = 0;
            this.toolTip.SetToolTip(this.buttonShutdown, "Shutdown");
            this.buttonShutdown.UseVisualStyleBackColor = true;
            this.buttonShutdown.Click += new System.EventHandler(this.buttonShutdown_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 175);
            this.Controls.Add(this.buttonLock);
            this.Controls.Add(this.buttonLogOff);
            this.Controls.Add(this.buttonHibernate);
            this.Controls.Add(this.buttonSleep);
            this.Controls.Add(this.buttonRestart);
            this.Controls.Add(this.buttonShutdown);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shutdowner";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonShutdown;
        private System.Windows.Forms.Button buttonRestart;
        private System.Windows.Forms.Button buttonSleep;
        private System.Windows.Forms.Button buttonHibernate;
        private System.Windows.Forms.Button buttonLogOff;
        private System.Windows.Forms.Button buttonLock;
        private System.Windows.Forms.ToolTip toolTip;
    }
}

