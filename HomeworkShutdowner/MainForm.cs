﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HomeworkShutdowner
{
    public partial class MainForm : Form
    {

        [DllImport("Powrprof.dll", CharSet = CharSet.Auto)]
        extern static bool SetSuspendState( bool bHibernate, bool bForce, bool bWakeupEventsDisabled);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        extern static bool ExitWindowsEx(uint uFlags, uint dwReason);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        extern static bool LockWorkStation();

        const uint LOGOFF = 0;


        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonLock_Click(object sender, EventArgs e)
        {
            LockWorkStation();
        }

        private void buttonShutdown_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to shutdown?", "Shutdown", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Process.Start("Shutdown", "/s /t 0");
        }

        private void buttonRestart_Click(object sender, EventArgs e)
        {
           if (MessageBox.Show("Do you really want to restart your computer?", "Restart", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Process.Start("Shutdown", "/r /t 0");
        }

        private void buttonSleep_Click(object sender, EventArgs e)
        {
            SetSuspendState(false, true, true);
        }

        private void buttonHibernate_Click(object sender, EventArgs e)
        {
            SetSuspendState(true, true, true);
        }

        private void buttonLogOff_Click(object sender, EventArgs e)
        {
            ExitWindowsEx(LOGOFF, 0);
        }
    }
}
